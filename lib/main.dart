import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'CalculatorController.dart';

void main() {
  runApp(MaterialApp(
      home:Calculator()
  ));
}

class Calculator extends StatelessWidget {
  var calculation = CalculatorController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[700],
      appBar: AppBar(
        backgroundColor: Colors.grey[600],
        centerTitle: true,
        title: Text('CALCULTOR',
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 20,
            letterSpacing: .5,
            color: Colors.white,
          ),
        ),
      ),
      body: Center(
        child: Container(
          color: Colors.blueGrey,
          width:380,
          margin: EdgeInsets.all(20),
          padding: EdgeInsets.all(35),
          child: Column(
            children: <Widget>[
              Container(
                color: Colors.white54,
                width:Get.width,
                height: 60,
                child: TextField(
                  controller: calculation.calculationText,
                  cursorColor: Colors.amber[800],
                  cursorHeight: 30,
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.deepOrange,width:3,),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color:Colors.teal[600],width: 3)
                    ),
                    hintText: '0',
                    hintStyle: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w600,
                      color:Colors.black,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 30,),
              Container(height: 5,width: 400, color:Colors.deepOrangeAccent),
              SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height:40,
                      width:78,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButtonCancle();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[300],
                      child: Text('Cancle',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.red[900],
                        fontWeight: FontWeight.w900,
                      ),
                      ),
                    ),
                  ),
                  Container(
                    height:40,
                    width:71,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButtonCross();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('X',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height:40,
                    width:71,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButtonDevider();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('/',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),

                ],
              ),
              SizedBox(height: 25,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height:40,
                    width:50,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButton7();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('7',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height:40,
                    width:50,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButton8();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('8',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height:40,
                    width:50,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButton9();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('9',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height:40,
                    width:50,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButtonMinus();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('-',
                        style: TextStyle(
                          fontSize: 35,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),

                ],
              ),
              SizedBox(height: 25,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height:40,
                    width:50,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButton4();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('4',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height:40,
                    width:50,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButton5();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('5',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height:40,
                    width:50,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButton6();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('6',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height:40,
                    width:50,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButtonPlus();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('+',
                        style: TextStyle(
                          fontSize: 35,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),

                ],
              ),
              SizedBox(height: 25,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height:40,
                    width:50,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButton1();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('1',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height:40,
                    width:50,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButton2();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('2',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height:40,
                    width:50,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButton3();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('3',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height:40,
                    width:50,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){},
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('%',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),

                ],
              ),
              SizedBox(height: 25,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height:40,
                    width:70,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){},
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('.',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height:40,
                    width:70,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButton0();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.grey[800],
                      child: Text('0',
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height:40,
                    width:80,
                    color: Colors.deepOrange[800],
                    child: RaisedButton(
                      onPressed: (){
                        calculation.pressButtonEqual();
                      },
                      elevation: 5,
                      splashColor: Colors.teal[400],
                      color: Colors.deepOrange[800],
                      child: Text('=',
                        style: TextStyle(
                          fontSize: 30,
                          color: Colors.amber[700],
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ),

                ],
              ),
              SizedBox(height: 25,),

            ],
          ),
        ),
      ),
    );
  }
}
