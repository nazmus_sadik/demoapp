import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class UserRatings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children:<Widget> [
          Container(
            margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
            padding: EdgeInsets.fromLTRB(20, 10, 35, 10),
            color: Colors.deepOrange[600],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children:<Widget> [
                Text('How was the food?',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 18,
                    color: Colors.white,
                  ),),
                SmoothStarRating(color: Colors.amber,starCount: 5,borderColor: Colors.grey[400],)
              ],
            ),

          ),
          Container( margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
            padding: EdgeInsets.fromLTRB(20, 10, 35, 30),
            decoration: BoxDecoration(
              border: Border.all(color:Colors.grey[400],width: 1,),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Do you recommand The Mirage for your friends and family?',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                    color: Colors.black,
                  ),),
                SizedBox(height: 5,),
                Row(
                  children:<Widget> [
                    RaisedButton(
                      onPressed: (){},
                      elevation: 5,
                      splashColor: Colors.teal[500],
                      color: Colors.deepOrange,
                      child: Text('YES',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 14,
                          color: Colors.white,
                        ),),

                    ),
                    SizedBox(width: 30,),
                    RaisedButton(
                      onPressed: (){},
                      elevation: 5,
                      splashColor: Colors.teal[500],
                      color: Colors.deepOrange,
                      child: Text('NO',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 14,
                          color: Colors.white,
                        ),),

                    ),
                  ],
                ),
                SizedBox(height: 35,),
                Text('Share your thoughts',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                    color: Colors.black,
                  ),),
                SizedBox(height: 5,),
                Container(
                  width: Get.width,
                  height: 80,
                  child: TextFormField(
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(width:1,color: Colors.blueGrey[600]),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(width:1,color: Colors.deepOrange[600]),
                      ),
                      hintText: 'Share your thoughts',


                      filled: true,
                      fillColor: Colors.grey[200],
                      hintStyle: TextStyle(
                        color:Colors.grey[400],
                      ),

                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}